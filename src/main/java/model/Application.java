package model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private UserRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        repository.deleteAll();

        // save a couple of customers
        repository.save(new User("Pedro", "919995566", "pedro@email.com"));
        repository.save(new User("Ricardo", "934442233", "ricardo@email.com"));

        // fetch all customers
        System.out.println("Users found with findAll():");
        System.out.println("-------------------------------");
        for (User user : repository.findAll()) {
            System.out.println(user);
        }
        System.out.println();

        // fetch an individual customer
        System.out.println("User found with findByName('Pedro'):");
        System.out.println("--------------------------------");
        System.out.println(repository.findByEmail("pedro@email.com"));

        System.out.println("Customers found with findByName('Ricardo'):");
        System.out.println("--------------------------------");
        for (User user : repository.findByName("Pedro")) {
            System.out.println(user);
        }
    }
}
